<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screenshot extends Model
{

    public $binPath = "";

    public $url = "";

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->binPath =  'node '.  base_path() . DIRECTORY_SEPARATOR . "index.js";
    }

    /**
     * pageres へのパスをセットする
     *
     * @param $newBinPath
     *
     * @return $this
     */
    public function setBinPath($newBinPath)
    {
        $this->binPath = $newBinPath;
        return $this;
    }

    /**
     * 幅のセット
     *
     * @param $newWidth
     *
     * @return $this
     */
    public function setURL($url)
    {
        $this->url = $url;

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setDelay($delay)
    {
        $this->delay = $delay;

        return $this;
    }
    /**
     * 幅のセット
     *
     * @param $newWidth
     *
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * 高さの設定
     *
     * @param $height
     *
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * スクリーンショットの取得
     *
     * @param string $path
     * @param string $css
     *
     * @return $this
     */
    public function getScreenshot($path, $css = "")
    {

        $shellscript = $this->binPath ."  --url=\"". $this->url . "\" --width=\"" . $this->width . "\" --height=\"" . $this->height ."\" --delay=\"" . $this->delay . "\" --dest=\"$path\" --name=\"$this->name\" --css=\"$css\"";

        return shell_exec(escapeshellcmd($shellscript));

    }

}
