<?php

namespace App\Http\Controllers;

use App\Page;
use App\Website;
use Illuminate\Http\Request;

use App\Http\Requests;

class WebsiteController extends Controller
{

    public function postRegister(Request $request)
    {
        $name = $request->input('name');

        $website      = new Website();
        $data["urls"] = "";
        $count        = $website->where('name', '=', $name)->get();

        if ($count->count()) {
            $website_id = $count->first()->id;
            $pages      = Page::where('website_id', '=', $website_id)->get()->lists('url')->toArray();
            $urls       = join("\n", $pages);
            $urls       = str_replace(array("\r\n"), "\n", $urls);

            return view('website/edit', [
                'website_id' => $count->first()->id,
                'urls'       => $urls
            ]);
        }

        $website->name = $name;
        $website->save();
        $data["website_id"] = $website->id;

        return view('website/edit', $data);
    }

    public function postEdit($website_id, Request $request)
    {

        $website = Website::where("id", '=', $website_id)->get()->first();

//        $website      = new Website();
        $data["urls"] = "";

        $pages = Page::where('website_id', '=', $website_id)->get()->lists('url')->toArray();
        $urls  = join("\n", $pages);
        $urls  = str_replace(array("\r\n"), "\n", $urls);

        return view('website/edit', [
            'website_id' => $website->id,
            'urls'       => $urls
        ]);
    }

    /**
     * @param $website_id
     * @param Request $request
     *
     * @return mixed
     */
    public function postDelete($website_id, Request $request)
    {


        $website = Website::where("id", "=", $website_id)->get();
        if ($website->count()) {
            $website->delete();
        }

        return redirect(url("/"))->with('message','Webサイトを削除しました');
    }
}
