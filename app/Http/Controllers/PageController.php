<?php

namespace App\Http\Controllers;

use App\Page;
use App\Screenshot;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    /**
     * ページの登録処理
     *
     * @param Request $request
     */
    public function postRegister(Request $request)
    {
        $urls       = $request->input("urls");
        $website_id = $request->input("website_id");
        $urlArray   = explode("\n", $urls);

        $filepath = storage_path("app/public/$website_id/");
        if ( ! Storage::exists($filepath)) {
            Storage::makeDirectory($filepath);
        }

        foreach ($urlArray as $url) {
            $page             = new Page();
            $page->url        = $url;
            $page->website_id = $website_id;
            $page->save();
            $page_id    = $page->id;
            $screenshot = new Screenshot();
            $name       = trim(str_replace(":", "_", str_replace("/", "_", $url)));
            $return     = $screenshot
                ->setURL(trim($url))
                ->setWidth(1980)
                ->setHeight(1000)
                ->setDelay(5)
                ->setName($name)
                ->getScreenshot($filepath);

            if ($return) {
                $screenshot->filepath = "storage/app/public/$website_id";
                $screenshot->page_id  = $page_id;
                $screenshot->name     = $name;
                $screenshot->save();
            } else {

            }
        }

        return redirect('screenshot/download')->with('website_id', $website_id);
    }

    /**
     * ページの登録
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postPageRegister(Request $request)
    {
        $url        = $request->input('url');
        $width      = $request->input('width');
        $delay      = $request->input('delay');
        $css        = $request->input('css');
        $website_id = $request->input("website_id");
        $filepath   = storage_path("app/public/$website_id/");

        if ( ! Storage::exists($filepath)) {
            Storage::makeDirectory($filepath);
        }

        $validator = \Validator::make($request->all(), [
            'url' => 'url|required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status'  => "fail",
                'message' => "正しいURLでありません",
            ]);
        }
        $page             = new Page();
        $page->url        = $url;
        $page->website_id = $website_id;
        $page->save();
        $page_id    = $page->id;
        $screenshot = new Screenshot();
        $name       = self::normalizeString($url);

        $return     = $screenshot
            ->setURL(trim($url))
            ->setWidth($width)
            ->setHeight(1000)
            ->setDelay($delay)
            ->setName($name)
            ->getScreenshot($filepath,$css);

        if ($return) {
            $screenshot->filepath = "app/public/$website_id/";
            $screenshot->page_id  = $page_id;
            $screenshot->name     = $name;
            $screenshot->save();
        } else {

        }

        return response()->json([
            'status'  => 'success',
            'message' => "スクリーンショットが作成されました。"
        ]);
    }
    public static function normalizeString ($str = '')
    {
        $str = strip_tags($str);
        $str = str_replace('#', '_', $str);
        $str = str_replace('?', '_', $str);
        $str = str_replace('=', '_', $str);
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = str_replace('.', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        if ( strlen($str) > 60 ){
            $str = substr($str,0,60);
        }
        return $str;
    }
}
