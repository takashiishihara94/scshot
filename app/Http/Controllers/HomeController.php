<?php

namespace App\Http\Controllers;

use App\Website;
use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    public function __construct()
    {

    }


    public function getIndex()
    {
        $website = Website::get();

        return view('welcome', ['websites' => $website]);
    }
}
