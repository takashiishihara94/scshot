<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;

class ScreenshotController extends Controller
{
    /**
     * リクエスト
     * @param integer $id
     * @param Request $request
     */
    public function getDownload($id, Request $request)
    {
        $website_id = $id;
        $data       = \DB::table('websites')
                         ->leftJoin('pages', 'websites.id', '=', 'pages.website_id')
                         ->leftJoin('screenshots', 'pages.id', '=', 'screenshots.page_id')
                         ->where('websites.id', '=', $website_id)
                         ->get();


        $table = \Table::create($data, ['website_id', 'url', 'name'])->render();

        return view('screenshot.download', [
            'website_id' => $website_id,
            'data'  => $data,
            'table' => $table
        ]);
    }


    public function getZipdownload($id,Request $request){

        $website_id = $id;
        $data       = \DB::table('websites')
                         ->leftJoin('pages', 'websites.id', '=', 'pages.website_id')
                         ->leftJoin('screenshots', 'pages.id', '=', 'screenshots.page_id')
                         ->where('websites.id', '=', $website_id)
                         ->get();

        $zipper = new \Chumper\Zipper\Zipper;

        $path = storage_path('app/public/' . $website_id);
        $files = glob($path);
        $zippath = 'app/public/screenshot-' . $website_id . ".zip";
        $zipper->make(storage_path($zippath))->folder("screenshots")->add($files)->close();
        $this->data["zippath"] = url("storage/".$zippath);
//        sleep(2);
        return response()->download(storage_path($zippath));

    }
}
