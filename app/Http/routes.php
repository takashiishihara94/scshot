<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');

// 登録しているサイトの一覧
Route::post('/list/', 'ListController@getIndex');

// ウェブサイトの登録
Route::post('/website/register', 'WebsiteController@postRegister');
Route::post('/website/edit/{id}', 'WebsiteController@postEdit');
Route::post('/website/delete/{id}', 'WebsiteController@postDelete');

// ページの登録
Route::post('/page/register', 'PageController@postRegister');

// スクリーンショットの作成
Route::post('/page/create', 'PageController@postPageRegister');

// スクショのダウンロード
Route::get('/screenshot/download/{id}', 'ScreenshotController@getDownload');
Route::get('/screenshot/zipdownload/{id}', 'ScreenshotController@getZipdownload');



