<!DOCTYPE html>
<html>
<head>
<title>Laravel</title>
<meta name="csrf-token" content="{{csrf_token()}}">
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <header>
        <h2 class="text-center" style="padding-top: 50px; padding-bottom: 50px;">
            <a href="{{url("/")}}" style="letter-spacing: 20px;color: #999;font-size: 30px;">スクショ！</a>
        </h2>
    </header>
    @yield("content")
</div>
</body>
</html>
