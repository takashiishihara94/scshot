@extends("layouts.base")
@section("content")
    <div class="container">
        <div class="content">
            {!! $table !!}
            <p class="text-center">
                <a href="{{url('screenshot/zipdownload/' . $website_id)}}" class="btn btn-primary btn-lg">
                    Zipでダウンロード
                </a>
            </p>
        </div>
    </div>
@stop
