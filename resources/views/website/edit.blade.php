@extends("layouts.base")
@section("content")

    <div class="container">
        <div class="content">
            {!! \Form::open(['url' => url("page/register"), 'method' => 'post']) !!}
            {!! Form::hidden('website_id',$website_id) !!}

            <p class="text-center">
                <span class="js-progress-text"></span>
            </p>
            <div class="progress js-progress" style="display: none">
                <div class="progress-bar progress-bar-success active progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">

                </div>
            </div>
            <h3>1. スクリーンショットを取るページURL入力してください。</h3>
            <p>
                <small>※ 改行で複数入力可能です</small>
            </p>

            {!! Form::textarea("urls",$urls,['class'=>'form-control js-urls', 'required' => 'true']) !!}

            <h3>2. 情報を入力してください</h3>
            <div class="row">
                <div class="col-lg-6" style="margin-bottom: 10px;">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">画面の幅</span>
                        {!! Form::number("width",1980,['class' => 'form-control js-width']) !!}
                    </div>
                </div>
                <div class="col-lg-6" style="margin-bottom: 10px;">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">ロードからの読み込み時間(秒)</span>
                        {!! Form::number("delay",2,['class' => 'form-control js-delay']) !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">カスタムCSS</span>
                        {!! Form::text("css","body{-webkit-font-smoothing: subpixel-antialiased !important}",['class' => 'form-control js-css']) !!}
                    </div>
                </div>
            </div>

            <p>　</p>
            <p class="text-center">
                {!! Form::button("登録", ['class'=>'btn btn-default js-submit','id' => 'js-page-submit'])!!}
            </p>
            {!! \Form::close() !!}

        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var ite = 0;
            var urls = [];
            var options = {
                width: "1980",
                delay: "2",
                css: "",
            };

            function submitPage(url) {
                $.ajax({
                    'url': "{{url("page/create")}}",
                    'type': 'post',
                    'data': {
                        'website_id': "{{$website_id}}",
                        'url': url,
                        'css': options.css,
                        'width': options.width,
                        'delay': options.delay,
                    }
                }).done(function (res) {
                    if (res.status == "success") {
                        ite++;
                        if (typeof urls[ite] !== 'undefined') {
                            startCreateScreenshot();
                            return false;
                        }
                        alert("すべて作成完了しました。");
                        window.location.href = "{{url('screenshot/download/'.$website_id)}}"
                    }
                });
            }

            function setUrls() {
                var tempurls = $('.js-urls').val();
                urls = tempurls.split("\n");
            }

            function startCreateScreenshot() {
                $(".js-progress-text").text((ite + 1) + "ページ目を作成中...");
                $('.js-progress .progress-bar').css('width', ((ite + 1 / urls.length) * 100) + "%");
                if (typeof urls[ite] !== 'undefined') {
                    submitPage(urls[ite]);
                    return false;
                }


            }

            $(document).on('click', '#js-page-submit', function (e) {
                e.preventDefault();
                $(".js-progress").fadeIn();
                options = {
                    width: $('.js-width').val(),
                    delay: $('.js-delay').val(),
                    css: $('.js-css').val(),
                };
                console.log(options);
                setUrls();

                startCreateScreenshot();
            })
        });
    </script>
@stop
