@extends("layouts.base")
@section("content")
    <div class="container">
        <div class="content">
            {!! \Form::open(['url' => url("website/register"), 'method' => 'post']) !!}
            <p>Webサイト名を入力</p>
            {!! Form::text("name",'',['class'=>'form-control', 'required' => 'true']) !!}
            <p>　</p>
            <p class="text-center">
                {!! Form::submit("登録",['class'=>'btn btn-default'])!!}
            </p>
            {!! \Form::close() !!}
            <h2 class="page-header">すでに登録されているWebサイト</h2>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="50">ID</th>
                        <th width="500">サイト名</th>
                        <th width="100">アクション</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($websites as $site)
                        <tr>
                            <td>{{$site->id}}</td>
                            <td>{{$site->name}}</td>
                            <td>
                                <p>
                                <form action="{{url('website/edit/'.$site->id)}}" method="post">
                                    {!! Form::hidden('_token',csrf_token()) !!}
                                    <button type="submit" class="btn btn-block btn-xs btn-info">編集</button>
                                </form>
                                </p>
                                <p>
                                <a href="{{url('screenshot/zipdownload/'.$site->id)}}" class="btn btn-xs btn-success btn-block">ダウンロード</a>
                                </p>

                                <form action="{{url('website/delete/'.$site->id)}}" method="post">
                                    {!! Form::hidden('_token',csrf_token()) !!}
                                    <button type="submit" class="btn btn-xs btn-danger btn-block">削除</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>
@stop
