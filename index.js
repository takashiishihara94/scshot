const Pageres = require('pageres');
const args = require('optimist').argv;
const util = require('util');
const url = args.url || "";
const width = args.width || "1980";
const delay = args.delay || "2";
const css = args.css || "";
const height = args.height || "1000";
const name = args.name || false;
const dest = args.dest || __dirname + "/storage/app/public/";

var options = {};
options.delay = delay;

if ( name ){
	options.filename = name;
}
if ( css ){
	options.css = css;
}

// console.log(options);
const pageres = new Pageres(options)
	.src(url, [width + 'x' + height])
	.dest(dest)
	.run()
	.then( ()=> {
		process.stdout.write('true');
	 });
